/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package PaqueteMascotas;

/**
 * 
 * @author Alex
 */
public class Perro {
    
    private String nombre;
    private String raza;
    private String color;
    private String peso;

    public Perro() {
    }

    public Perro(String nombre, String raza, String color, String peso) {
        this.nombre = nombre;
        this.raza = raza;
        this.color = color;
        this.peso = peso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }
    
    

}
